/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
// export const transformArg = (isTrue, arg) => {
//   return `${isTrue ? /*":" + arg + '="$props.' + arg + '"'*/ arg : ""}`;
// };

// export const transformText = (isText, arg) => {
//   return `${
//     isText === "" || isText === null ? "" : ":" + arg + '="$props.' + arg + '"'
//   }`;
// };

const kebabise = (string) => {
  // uppercase after a non-uppercase or uppercase before non-uppercase
  const upper =
    /(?<!\p{Uppercase_Letter})\p{Uppercase_Letter}|\p{Uppercase_Letter}(?!\p{Uppercase_Letter})/gu;
  return string.replace(upper, "-$&").replace(/^-/, "").toLowerCase();
};

export const kebabiseArgs = (args) => {
  const argsKebabCase = {};
  Object.keys(args)
    .filter(
      (key) =>
        ![
          "default",
          "header",
          "content",
          "body",
          "footer",
          "categorias",
          "redesSociais",
          "logo",
          "footerImagens",
          "slotTemplate",
        ].includes(key)
    )
    .forEach((key) => {
      argsKebabCase[kebabise(key)] =
        typeof args[key] === "boolean" && !args[key] ? null : args[key];
    });
  return argsKebabCase;
};

// export const transformArgKebab = (isTrue, arg) => {
//   return `${isTrue ? ":" + kebabise(arg) + '="$props.' + arg + '"' : ""}`;
// };

export const templateSourceCode = (
  templateSource,
  args,
  argTypes,
  replacing = 'v-bind="args"'
) => {
  const componentArgs = {};
  for (const [k, t] of Object.entries(argTypes)) {
    const val = args[k];
    if (
      typeof val !== "undefined" &&
      !(typeof val === "boolean" && val === false) &&
      t.table &&
      t.table.category === "props" &&
      val !== t.defaultValue
    ) {
      componentArgs[k] = val;
    }
  }

  const propToSource = (key, val) => {
    const type = typeof val;
    switch (type) {
      case "boolean":
        return val ? kebabise(key) : "";
      default:
        return `${kebabise(key)}="${val}"`;
    }
  };

  const propsAndValuesArray = Object.keys(componentArgs)
    .filter(
      (key) =>
        ![
          "default",
          "header",
          "content",
          "body",
          "footer",
          "categorias",
          "redesSociais",
          "logo",
          "footerImagens",
          "slotTemplate",
        ].includes(key)
    )
    .map((key) => propToSource(key, args[key]));

  return templateSource.replaceAll(
    propsAndValuesArray.length ? replacing : " " + replacing,
    propsAndValuesArray.join(" ")
  );
};
