/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount } from "@vue/test-utils";
import BrFooterSocialNetworkItem from "./FooterSocialNetworkItem.vue";

describe("BrFooterSocialNetworkItem", () => {
  test("Testa se o uso da classe mr-3 está sendo renderizado.", () => {
    const wrapper = shallowMount(BrFooterSocialNetworkItem);
    expect(wrapper.find(".mr-3").exists()).toBe(true);
  });

  test("Testa se o slot está sendo carregado dentro do elemento de classe mr-3.", () => {
    const wrapper = shallowMount(BrFooterSocialNetworkItem, {
      slots: {
        default: '<div id="teste"></div>',
      },
    });
    expect(wrapper.find(".mr-3 #teste").exists()).toBe(true);
  });
});
