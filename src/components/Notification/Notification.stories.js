/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrNotification from "./Notification.ce.vue";
import { kebabiseArgs } from "../../util/Utils.js";

const defaultSlotContent = `
  <br-tab>
    <br-tab-item title="Aba 1">
      <br-list>
        <button class="br-item" type="button"><i class="fas fa-home" aria-hidden="true"></i>Link de Acesso</button>
        <br-divider></br-divider>
        <button class="br-item" type="button"><i class="fas fa-heartbeat" aria-hidden="true"></i>Link de Acesso</button>
        <br-divider></br-divider>
        <button class="br-item" type="button"><i class="fas fa-user" aria-hidden="true"></i>Link de Acesso</button>
      </br-list>
    </br-tab-item>
    <br-tab-item title="Aba 2">
      <br-list>
      <button class="br-item" type="button">
        <span class="br-tag status small warning"></span>
        <span class="text-bold">Título</span>
        <span class="text-medium mb-2">25 de out</span>
        <span>Texto da notificação.</span>
      </button>
      <br-divider></br-divider>
      <button class="br-item" type="button">
        <span class="text-bold">Título</span>
        <span class="text-medium mb-2">24 de out</span>
        <span>Texto da notificação.</span>
      </button>
      <br-divider></br-divider>
      <button class="br-item" type="button">
        <span class="br-tag status small warning"></span>
        <span class="text-bold">Título</span>
        <span class="text-medium mb-2">03 de out</span>
        <span>Texto da notificação.</span>
      </button>
      <br-divider></br-divider>
      <button class="br-item" type="button">
        <span class="text-bold">Título</span>
        <span class="text-medium mb-2">16 de mai</span>
        <span>Texto da notificação.</span>
      </button>
      </br-list>
    </br-tab-item>
  </br-tab>
`;

export default {
  title: "Dsgov/br-notification",
  component: BrNotification,
  argTypes: {
    title: {
      defaultValue: "Título da notificação",
    },
    subtitle: {
      defaultValue: "Subtítulo da notificação",
    },
    default: {
      description:
        "**[OBRIGATÓRIO]** Conteúdo da notificação, que deve ser passado por slot.",
      defaultValue: defaultSlotContent,
      control: "text",
      type: {
        required: true,
      },
    },
  },
};

const TemplateDefault = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-notification v-bind="args">${args.default}</br-notification>`,
});

export const Simple = TemplateDefault.bind({});
