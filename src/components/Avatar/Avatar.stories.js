/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrAvatar from "./Avatar.ce.vue";
import { kebabiseArgs } from "../../util/Utils.js";

export default {
  title: "Dsgov/br-avatar",
  component: BrAvatar,
  argTypes: {
    name: {
      control: "text",
    },
    iconic: {},
    image: {
      control: "text",
    },
    density: {
      control: { type: "select", options: ["small", "medium", "large"] },
    },
  },
};

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-avatar v-bind="args"></br-avatar>`,
});

export const Iconico = Template.bind({});
Iconico.args = {
  name: "Jhon Doe",
  iconic: true,
};

export const Fotografico = Template.bind({});
Fotografico.args = {
  name: "Jhon Doe",
  iconic: false,
  image: "https://picsum.photos/id/823/400",
};

export const Letra = Template.bind({});
Letra.args = {
  name: "Jhon Doe",
  iconic: false,
};
