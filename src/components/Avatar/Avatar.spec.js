/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount, createLocalVue } from "@vue/test-utils";
import BrAvatar from "./Avatar.ce.vue";

const localVue = createLocalVue();
localVue.component("br-avatar", BrAvatar);

describe("BrAvatar", () => {
  test("Deve verificar se o tipo do avatar é Letra.", () => {
    const textName = "Jhon Doe";
    const wrapper = shallowMount(BrAvatar, {
      localVue,
      propsData: {
        name: textName,
      },
    });
    expect(wrapper.text()).toMatch("J");
  });

  test("Deve verificar se o tipo do avatar é iconico'.", () => {
    const typeAvatar = true;
    const wrapper = shallowMount(BrAvatar, {
      localVue,
      propsData: {
        iconic: typeAvatar,
      },
    });
    expect(wrapper.find("i.fas.fa-user").exists()).toBe(true);
  });

  test("Deve verificar se o tipo do avatar é fotográfico'.", () => {
    const urlImageAvatar = "https://picsum.photos/id/823/400";
    const wrapper = shallowMount(BrAvatar, {
      localVue,
      propsData: {
        image: urlImageAvatar,
      },
    });
    expect(wrapper.find("img[src='" + urlImageAvatar + "']").exists()).toBe(
      true
    );
  });

  test("Deve verificar se a densidade aplica é small", () => {
    const densitySmall = "small";
    const wrapper = shallowMount(BrAvatar, {
      localVue,
      propsData: {
        name: "Jhon Doe",
        density: densitySmall,
      },
    });
    expect(wrapper.find("span.br-avatar.small").exists()).toBe(true);
  });

  test("Deve verificar se a densidade aplica é medium", () => {
    const densityMedium = "medium";
    const wrapper = shallowMount(BrAvatar, {
      localVue,
      propsData: {
        name: "Jhon Doe",
        density: densityMedium,
      },
    });
    expect(wrapper.find("span.br-avatar.medium").exists()).toBe(true);
  });

  test("Deve verificar se a densidade aplica é large", () => {
    const densitylarge = "large";
    const wrapper = shallowMount(BrAvatar, {
      localVue,
      propsData: {
        name: "Jhon Doe",
        density: densitylarge,
      },
    });
    expect(wrapper.find("span.br-avatar.large").exists()).toBe(true);
  });
});
