/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrInput from "./Input.ce.vue";
import { kebabiseArgs } from "../../util/Utils.js";

export default {
  title: "Dsgov/br-input",
  component: BrInput,
  argTypes: {
    label: {
      control: "text",
    },
    placeholder: {
      control: "text",
    },
    density: {
      control: { type: "select", options: ["small", "medium", "large"] },
    },
    state: {
      control: {
        type: "select",
        options: ["info", "warning", "danger", "success"],
      },
    },
    disabled: {
      control: "boolean",
    },
    autofocus: {
      control: "boolean",
    },
    icon: {
      control: "text",
    },
  },
};

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-input v-bind="args"></br-input>`,
});

export const base = Template.bind({});
base.args = {
  label: "Label",
  placeholder: "Placeholder",
  disabled: false,
  state: "",
};

export const danger = Template.bind({});
danger.args = {
  label: "CPF",
  placeholder: "CPF inexistente.",
  state: "danger",
  disabled: false,
};

export const success = Template.bind({});
success.args = {
  label: "Nome Completo",
  placeholder: "Fulano Beltrano",
  state: "success",
  disabled: false,
};

export const warning = Template.bind({});
warning.args = {
  label: "CPF",
  placeholder: "Digite somente números",
  state: "warning",
  disabled: false,
};

export const info = Template.bind({});
info.args = {
  label: "CPF",
  placeholder: "Digite somente números",
  state: "info",
  disabled: false,
};

export const icon = Template.bind({});
icon.args = {
  label: "Senha",
  placeholder: "Digite a senha de 8 a 11 dígitos",
  disabled: false,
  ispassword: true,
};
