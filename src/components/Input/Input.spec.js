/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount, createLocalVue } from "@vue/test-utils";
import BrInput from "./Input.ce.vue";

const localVue = createLocalVue();
localVue.component("br-input", BrInput);

const densidades = ["large", "medium", "small"];
const cores = ["info", "warning", "danger", "success"];

describe("Input", () => {
  test('find Input component and class "br-input"', () => {
    const wrapper = shallowMount(BrInput);
    const input = wrapper.findComponent({ ref: "brinput" });
    expect(input.exists()).toBe(true);

    expect(wrapper.classes("br-input")).toBe(true);
  });

  test("setValue on input", async () => {
    const wrapper = shallowMount(BrInput);
    const textInput = wrapper.find('input[type="text"]');
    await textInput.setValue("Inserindo texto no input");
    expect(wrapper.find('input[type="text"]').element.value).toBe(
      "Inserindo texto no input"
    );
  });

  test("set label attribute", () => {
    const rotulo = "Label do input";
    const wrapper = shallowMount(BrInput, {
      localVue,
      propsData: {
        label: rotulo,
      },
    });
    expect(wrapper.text()).toMatch(rotulo);
  });

  /*test('set icon', () => {
    const icone = 'fa-car'
    const wrapper = shallowMount(BrInput, {
      localVue,
      propsData: {
        icon: icone
      }
    })
    expect(wrapper.find('.has-icon').exists()).toBe(true)
  })*/

  test("has the expected html structure", () => {
    const wrapper = shallowMount(BrInput);
    expect(wrapper.element).toMatchSnapshot();
  });

  densidades.forEach((densidade) => {
    test(`set density attribute ${densidade}`, () => {
      const wrapper = shallowMount(BrInput, {
        localVue,
        propsData: {
          density: densidade,
        },
      });
      expect(wrapper.find(`.${densidade}`).exists()).toBe(true);
      expect(wrapper.element).toMatchSnapshot();
    });
  });

  cores.forEach((cor) => {
    test(`set state attribute ${cor}`, () => {
      const wrapper = shallowMount(BrInput, {
        localVue,
        propsData: {
          state: cor,
        },
      });
      expect(wrapper.find(`.${cor}`).exists()).toBe(true);
      expect(wrapper.element).toMatchSnapshot();
    });
  });
});
