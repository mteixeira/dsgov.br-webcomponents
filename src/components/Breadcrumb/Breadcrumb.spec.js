/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount, createLocalVue } from "@vue/test-utils";
import Breadcrumb from "./Breadcrumb.ce.vue";

const localVue = createLocalVue();
localVue.component("br-breadcrumb", Breadcrumb);

describe("br-breadcrumb", () => {
  test("Testa se o uso das classes br-breadcrumb está sendo renderizado.", () => {
    const wrapper = shallowMount(Breadcrumb);
    expect(wrapper.find(".br-breadcrumb").exists()).toBe(true);
  });

  test("Testa se o atributo aria-label está com o valor 'Default'", () => {
    const wrapper = shallowMount(Breadcrumb, {
      localVue,
      propsData: {
        title: "Default",
      },
    });

    // console.log("wrapper.attributes()=", wrapper.attributes());

    expect(wrapper.attributes("aria-label")).toBe("Default");
  });
});
