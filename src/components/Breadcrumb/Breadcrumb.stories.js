/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrBreadcrumb from "./Breadcrumb.ce.vue";
import { kebabiseArgs } from "../../util/Utils.js";

const defaultSlotContent = `
  <br-breadcrumb-crumb label="Início" home></br-breadcrumb-crumb>
  <br-breadcrumb-crumb label="Funcionalidade X" :onClick="breadCrumbClick"></br-breadcrumb-crumb>
  <br-breadcrumb-crumb label="DSGov" target="_blank"
      href="https://dsgov.estaleiro.serpro.gov.br/"></br-breadcrumb-crumb>
  <br-breadcrumb-crumb label="Componente" active></br-breadcrumb-crumb>
`;

export default {
  title: "DSGOV/br-breadcrumb",
  component: BrBreadcrumb,
  argTypes: {
    // o nome do slot default é "default" e pode ser referenciado como chave dos argTypes
    title: {
      control: "text",
    },
    default: {
      description:
        "**[OBRIGATÓRIO]** Conteúdo da notificação, que deve ser passado por slot.",
      defaultValue: defaultSlotContent,
      control: "text",
      type: {
        required: true,
      },
    },
  },
};

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-breadcrumb v-bind="args">${args.default}</br-breadcrumb>`,
  methods: {
    breadCrumbClick: () => {
      alert("Breadcrumb foi clicado!");
    },
  },
});

export const Default = Template.bind({});
Default.args = {
  title: "Breadcrumb Padrão",
  default: defaultSlotContent,
};
