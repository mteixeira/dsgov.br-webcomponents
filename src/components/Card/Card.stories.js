/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import Card from "./Card.ce.vue";
import { kebabiseArgs } from "../../util/Utils.js";

const slotContent = `
    <br-card-content slot="content">
      <img src="https://picsum.photos/id/0/500" alt="Imagem de exemplo"/>
    </br-card-content>
`;

const slotContent2 = `
    <br-card-content slot="content">
      <p>
        Lorem ipsum dolor sit, amet consectetur adipisicing elit.
        Tempore perferendis nam porro atque ex at, numquam non optio ab
        eveniet error vel ad exercitationem, earum et fugiat recusandae
        harum? Assumenda.
      </p>
    </br-card-content>
`;

const slotHeader = `
    <br-card-header slot="header">
      <div class="d-flex">
        <br-avatar imgSrc="https://picsum.photos/id/823/400" />
        <div class="ml-3">
          <p class="h5 text-primary-default mb-0 mt-0 ">Maria Amorim</p>
          <span>UX Designer</span>
        </div>
        <div class="ml-auto">
          <br-button circle icon="ellipsis-v" />
        </div>
      </div>
    </br-card-header>
`;
const slotFooter = `
    <br-card-footer slot="footer">
      <div class="d-flex">
        <div>
          <br-button label="Button" />
        </div>
        <div class="ml-auto">
          <br-button circle icon="share-alt" />
          <br-button circle icon="heart" />
        </div>
      </div>
    </br-card-footer>
`;

export default {
  title: "Dsgov/br-card",
  component: Card,
  argTypes: {
    header: {
      control: "text",
      description:
        "** header slot - exclusivo para títulos, subtítulos, ícones, avatares e tags.",
    },
    content: {
      control: "text",
      description:
        "** content slot - qualquer componente é aceitável, exceto componentes relacionados à navegação, como: carrossel, paginação, abas e menu.",
    },
    footer: {
      control: "text",
      description: "** footer slot - exclusivo para botões e links.",
    },
  },
};

const TemplateContent = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<div class="col-sm-6 col-md-4 col-lg-3">
  <br-card v-bind="args">${args.content}  </br-card>
</div>
  `,
});

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<div class="col-sm-6 col-md-4 col-lg-3">
  <br-card v-bind="args">${args.header}${args.content}${args.footer}  </br-card>
</div>
  `,
});

const TemplateHeader = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<div class="col-sm-6 col-md-4 col-lg-3">
  <br-card v-bind="args">${args.header}  </br-card>
</div>
  `,
});

const TemplateFooter = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<div class="col-sm-6 col-md-4 col-lg-3">
  <br-card v-bind="args">${args.footer}  </br-card>
</div>
  `,
});

export const Simples = TemplateContent.bind({});
Simples.args = {
  content: slotContent,
};

export const HeaderSlot = TemplateHeader.bind({});
HeaderSlot.args = {
  header: slotHeader,
};

export const ContentSlot = TemplateContent.bind({});
ContentSlot.args = {
  content: slotContent2,
};
//ContentSlot.parameters = { controls: { exclude: ['slotTemplate'] } }

export const FooterSlot = TemplateFooter.bind({});
FooterSlot.args = {
  footer: slotFooter,
};
//FooterSlot.parameters = { controls: { exclude: ['slotTemplate'] } }

export const Completo = Template.bind({});
Completo.args = {
  header: slotHeader,
  content: slotContent2,
  footer: slotFooter,
};
//Completo.parameters = { controls: { exclude: ['slotTemplate'] } }

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true,
  header: slotHeader,
  content: slotContent2,
  footer: slotFooter,
};
//Disabled.parameters = { controls: { exclude: ['slotTemplate'] } }

export const Hover = Template.bind({});
Hover.args = {
  hover: true,
  header: slotHeader,
  content: slotContent2,
  footer: slotFooter,
};
//Hover.parameters = { controls: { exclude: ['slotTemplate'] } }

export const Expanded = Template.bind({});
Expanded.args = {
  dataExpanded: "on",
  header: slotHeader,
  content: slotContent2,
  footer: slotFooter,
};
//Expanded.parameters = { controls: { exclude: ['slotTemplate'] } }

export const Retracted = Template.bind({});
Retracted.args = {
  dataExpanded: "off",
  header: slotHeader,
  content: slotContent2,
  footer: slotFooter,
};
//Retracted.parameters = { controls: { exclude: ['slotTemplate'] } }
