/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount, createLocalVue } from "@vue/test-utils";

import Card from "../Card/Card.ce.vue";
import CardHeader from "../Card/CardHeader.ce.vue";
import CardContent from "../Card/CardContent.ce.vue";
import CardFooter from "../Card/CardFooter.ce.vue";
import BrAvatar from "../Avatar/Avatar.ce.vue";
import Button from "../Button/Button.ce.vue";

const localVue = createLocalVue();
localVue.component("br-card", Card);
localVue.component("br-card-header", CardHeader);
localVue.component("br-card-content", CardContent);
localVue.component("br-card-footer", CardFooter);
localVue.component("br-avatar", BrAvatar);
localVue.component("br-button", Button);

describe("Card", () => {
  test("Testa se a prop disable está sendo renderizada.", () => {
    const wrapper = shallowMount(Card, {
      propsData: {
        disable: true,
      },
    });

    expect(wrapper.find(".br-card").attributes("disable")).toBe("true");
  });

  test("Testa a renderização do slot header e avatar.", () => {
    const wrapper = shallowMount(Card, {
      localVue,
      slots: {
        header: `
            <br-card-header slot="header">
              <div class="d-flex">
                <br-avatar imgSrc="https://picsum.photos/id/823/400" />
                <div class="ml-3">
                  <p class="h5 text-primary-default mb-0">Maria Amorim</p>
                  <span>UX Designer</span>
                </div>
                <div class="ml-auto">
                  <button type="circle">
                    <span slot="button-icon">
                      <i class="fas fa-ellipsis-v" aria-hidden="true"></i>
                    </span>
                  </button>
                </div>
              </div>
            </br-card-header>
        `,
      },
    });

    expect(wrapper.findAllComponents(CardHeader).is(CardHeader)).toBe(true);
    expect(wrapper.findAllComponents(CardHeader).is(CardHeader)).toBe(true);
  });

  test("Testa a renderização do slot content.", () => {
    const wrapper = shallowMount(Card, {
      localVue,
      slots: {
        content: `
          <br-card-content slot="content">
            <p>
              Lorem ipsum dolor sit, amet consectetur adipisicing elit.
              Tempore perferendis nam porro atque ex at, numquam non
              optio ab eveniet error vel ad exercitationem, earum et
              fugiat recusandae harum? Assumenda.
            </p>
        </br-card-content>
        `,
      },
    });

    expect(wrapper.findAllComponents(CardContent).is(CardContent)).toBe(true);
  });

  test("Testa a renderização do slot footer e button.", () => {
    const wrapper = shallowMount(Card, {
      localVue,
      slots: {
        footer: `
          <br-card-footer slot="footer">
            <div class="d-flex">
              <div>
                <button label="Button" />
              </div>
              <div class="ml-auto">
                <button type="circle">
                  <span slot="button-icon">
                    <i class="fas fa-share-alt" aria-hidden="true"></i>
                  </span>
                </button>
                <button type="circle">
                  <span slot="button-icon">
                    <i class="fas fa-heart" aria-hidden="true"></i>
                  </span>
                </button>
              </div>
            </div>
          </br-card-footer>
        `,
      },
    });

    expect(wrapper.findAllComponents(CardFooter).is(CardFooter)).toBe(true);
    //expect(wrapper.findAllComponents(Button).is(Button)).toBe(true)
  });
});
