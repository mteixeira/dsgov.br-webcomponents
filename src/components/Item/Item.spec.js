/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount, createLocalVue } from "@vue/test-utils";
import Item from "./Item.ce.vue";
import List from "../List/List.ce.vue";

const localVue = createLocalVue();
localVue.component("br-list", List);

describe("br-item", () => {
  test("Testa se a prop active está renderizando.", () => {
    const wrapper = shallowMount(Item, {
      propsData: { active: true },
    });
    expect(wrapper.find(".active").exists()).toBe(true);
  });

  test("Testa se a prop hover está renderizando", () => {
    const wrapper = shallowMount(Item, {
      propsData: { hover: true },
    });
    expect(wrapper.find("div[data-toggle='selection']").exists()).toBe(true);
  });

  test("Testa se a prop disabled está renderizando", () => {
    const wrapper = shallowMount(Item, {
      propsData: { disabled: true },
    });
    expect(wrapper.find(".disabled").exists()).toBe(true);
  });

  test("Testa se a prop selected está renderizando.", () => {
    const wrapper = shallowMount(Item, {
      propsData: { selected: true },
    });
    expect(wrapper.find(".selected").exists()).toBe(true);
  });

  // test("Testa se a prop title é exibida, caso aṕenas em que o br-list pai é dataToggle.", () => {
  //   const titleExample = "title example";
  //   const wrapper = shallowMount(Item, {
  //     propsData: {
  //       collapsible: true,
  //       title: titleExample,
  //     },
  //   });
  //   // expect(wrapper.html()).toMatch('data-enabled="' + labelChecked + '"');
  //   expect(wrapper.text()).toMatch(titleExample);
  // });

  test("Testa se o evento toggle-selected foi disparado", async () => {
    const wrapper = shallowMount(Item, {
      propsData: { selected: false },
    });
    wrapper.setProps({ selected: true });
    await wrapper.emitted("toggle-selected");
    expect(wrapper.emitted("toggle-selected")).toBeTruthy();
  });
});
