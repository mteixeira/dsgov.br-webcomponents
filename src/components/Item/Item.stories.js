/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrItem from "./Item.ce.vue";
import { kebabiseArgs } from "../../util/Utils.js";

const defaultSimpleText = `Um texto simples.`;

const defaultImageAndText = `
  <div class="row align-items-center">
    <div class="col-auto"><img class="rounded" src="https://picsum.photos/40/40" alt="imagem de exemplo 1" /></div>
    <div class="col"><span>Item 1 - uma imagem e um texto (sem elemento de formulário)</span></div>
  </div>
`;

const defaultListDataToggle = `
    <br-list>
      <ul>
        <li>Info 1</li>
        <li>Info 2</li>
        <li>Info 3</li>
      </ul>
    </br-list>
`;

export default {
  title: "Dsgov/br-item",
  component: BrItem,
  argTypes: {
    active: {
      control: "boolean",
    },
    hover: {
      control: "boolean",
    },
    disabled: {
      control: "boolean",
    },
    selected: {
      control: "boolean",
    },
    "toggle-selected": {
      description:
        "**[OPCIONAL]** Evento disparado quando a prop selected é alterada.",
    },
    "toggle-open": {
      description:
        "**[OPCIONAL]** Evento disparado quando a prop open é alterada.",
    },
    default: {
      control: "text",
      default: "Texto do item",
      description:
        "**[OBRIGATÓRIO]** Conteúdo do item, que deve ser passado por slot.",
    },
  },
};

const TemplateDefault = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-item v-bind="args">${args.default}</br-item>`,
});

export const SimpleText = TemplateDefault.bind({});
SimpleText.args = {
  default: defaultSimpleText,
};

export const ImageAndText = TemplateDefault.bind({});
ImageAndText.args = {
  default: defaultImageAndText,
};

const TemplateListDataToggle = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-list data-toggle="true" title="${args.titleList}">
  <br-item v-bind="args">${args.default}  </br-item>
</br-list>`,
});

export const ListDataToggle = TemplateListDataToggle.bind({});
ListDataToggle.args = {
  titleList: "Isto é um <br-list data-toggle> que contém um <br-item>",
  title: "Um título para um item com mais informações",
  default: defaultListDataToggle,
};

const TemplateButton = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-item v-bind="args">
  <br-button label="br-button dentro de um br-item" type="primary" />
</br-item>`,
});

export const Button = TemplateButton.bind({});

const TemplateMultipleCheckboxes = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<div class="container">
  <div class="row">
    <div class="col-5">
      <br-item v-bind="args">
        <div class="br-checkbox">
          <input id="check-01" type="checkbox" v-model="args.selected1" />
          <label for="check-01">Item 1 - checkbox com html puro (aguardando desenvolvimento do componente br-checkbox)</label>
        </div>
      </br-item>
      <br-divider />
      <br-item v-bind="args">
        <div class="br-checkbox">
          <input id="check-02" type="checkbox" v-model="args.selected2" />
          <label for="check-02">Item 2 - checkbox com html puro (aguardando desenvolvimento do componente br-checkbox)</label>
        </div>
      </br-item>
      <br-divider />
      <br-item v-bind="args">
        <div class="br-checkbox">
          <input id="check-03" type="checkbox" v-model="args.selected3" />
          <label for="check-03">Item 3 - checkbox com html puro (aguardando desenvolvimento do componente br-checkbox)</label>
        </div>
      </br-item>
    </div>
    <br-divider vertical />
    <div class="col-5">
      <br-item v-bind="args">
        <div class="br-checkbox">
          <input id="check-04" type="checkbox" v-model="args.selected4" />
          <label for="check-04">Item 4 - checkbox com html puro (aguardando desenvolvimento do componente br-checkbox)</label>
        </div>
      </br-item>
      <br-divider />
      <br-item v-bind="args">
        <div class="br-checkbox">
          <input id="check-05" type="checkbox" v-model="args.selected5" />
          <label for="check-05">Item 5 - checkbox com html puro (aguardando desenvolvimento do componente br-checkbox)</label>
        </div>
      </br-item>
      <br-divider />
      <br-item v-bind="args">
        <div class="br-checkbox">
          <input id="check-06" type="checkbox" v-model="args.selected6" />
          <label for="check-06">Item 6 - checkbox com html puro (aguardando desenvolvimento do componente br-checkbox)</label>
        </div>
      </br-item>
    </div>
  </div>
</div>
  `,
});

export const MultipleCheckboxes = TemplateMultipleCheckboxes.bind({});
MultipleCheckboxes.args = {
  selected1: false,
  selected2: false,
  selected3: false,
  selected4: false,
  selected5: false,
  selected6: false,
  item1: "selected1",
};
