/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount, createLocalVue } from "@vue/test-utils";
import Checkbox from "./Checkbox.ce.vue";

const localVue = createLocalVue();
localVue.component("br-checkbox", Checkbox);

describe("Checkbox", () => {
  test("it renders br-checkbox", () => {
    const wrapper = shallowMount(Checkbox);
    expect(wrapper.classes("br-checkbox")).toBe(true);
  });

  test("set label attribute", () => {
    const rotulo = "Label do checkbox";
    const wrapper = shallowMount(Checkbox, {
      localVue,
      propsData: {
        label: rotulo,
      },
    });
    expect(wrapper.text()).toMatch(rotulo);
  });

  test("set inline attribute", () => {
    const wrapper = shallowMount(Checkbox, {
      localVue,
      propsData: {
        inline: true,
      },
    });
    expect(wrapper.find(".br-checkbox.d-inline").exists()).toBe(true);
  });

  test("set disabled attribute", () => {
    const wrapper = shallowMount(Checkbox, {
      localVue,
      propsData: {
        disabled: true,
      },
    });
    expect(wrapper.find(".br-checkbox").props().disabled).toBe(true);
  });

  test("set valid attribute", () => {
    const wrapper = shallowMount(Checkbox, {
      localVue,
      propsData: {
        valid: true,
      },
    });
    expect(wrapper.find(".br-checkbox").props().valid).toBe(true);
  });

  test("set invalid attribute", () => {
    const wrapper = shallowMount(Checkbox, {
      localVue,
      propsData: {
        invalid: true,
      },
    });
    expect(wrapper.find(".br-checkbox").props().invalid).toBe(true);
  });

  test("set indeterminate checkbox", () => {
    const wrapper = shallowMount(Checkbox, {
      localVue,
      propsData: {
        indeterminate: true,
      },
    });
    expect(wrapper.find(".br-checkbox").props().indeterminate).toBe(true);
  });

  test("set checked checkbox", () => {
    const wrapper = shallowMount(Checkbox, {
      localVue,
      propsData: {
        checked: true,
      },
    });
    expect(wrapper.find(".br-checkbox").props().checked).toBe(true);
  });
});
