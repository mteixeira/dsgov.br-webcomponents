/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import Checkbox from "../Checkbox/Checkbox.ce.vue";
import { kebabiseArgs } from "../../util/Utils.js";

export default {
  title: "Dsgov/br-checkbox",
  component: Checkbox,
  argTypes: {},
};

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-checkbox v-bind="args"></br-checkbox>`,
});

export const Base = Template.bind({});
Base.args = {
  label: "Label do checkbox",
  id: "base",
  name: "base",
  model: "check1",
};

export const WithoutLabel = Template.bind({});
WithoutLabel.args = {
  id: "withoutlabel",
  vModel: "check2",
};

export const Checked = Template.bind({});
Checked.args = {
  label: "Label do checkbox",
  id: "checked-01",
  checked: "checked",
  vModel: "check3",
};

export const Invalid = Template.bind({});
Invalid.args = {
  label: "Label do checkbox",
  id: "invalid-01",
  invalid: true,
  vModel: "check4",
};

export const Valid = Template.bind({});
Valid.args = {
  label: "Label do checkbox",
  id: "valid-01",
  valid: true,
};

export const Disabled = Template.bind({});
Disabled.args = {
  label: "Label do checkbox",
  id: "disabled-01",
  disabled: true,
};

const TemplateMultiple = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<div>
  <p class="label mb-0">Rótulo</p>
  <p class="text-down-01">Informações adicionais</p>
  <div class="mb-1">
    <br-checkbox id="Unchecked-padrao" label="Unchecked"></br-checkbox>
  </div>
  <div class="mb-1">
    <br-checkbox id="Checked-padrao" label="Checked" checked="checked"></br-checkbox>
  </div>
  <div class="mb-1">
    <br-checkbox id="valid-padrao" label="Valid" valid></br-checkbox>
  </div>
  <div class="mb-1">
    <br-checkbox id="invalid-padrao" label="Invalid" invalid></br-checkbox>
  </div>
  <div class="mb-1">
    <br-checkbox id="disabled-padrao" label="Disabled" disabled></br-checkbox>
  </div>
</div>`,
});

export const Multiple = TemplateMultiple.bind({});
Multiple.parameters = {
  controls: { exclude: ["label", "disabled", "inline", "id", "value"] },
};

const TemplateInline = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<div>
  <p class="label mb-0">Rótulo</p>
  <p class="text-down-01">Informações adicionais</p>
  <br-checkbox id="Unchecked-inline" label="Unchecked" inline></br-checkbox>
  <br-checkbox id="Checked-inline" label="Checked" checked="checked" inline></br-checkbox>
  <br-checkbox id="valid-inline" label="Valid" valid inline></br-checkbox>
  <br-checkbox id="invalid-inline" label="Invalid" invalid inline></br-checkbox>
  <br-checkbox id="disabled-inline" label="Disabled" disabled inline></br-checkbox>
</div>
  `,
});

export const Inline = TemplateInline.bind({});
Inline.parameters = {
  controls: { exclude: ["label", "disabled", "inline", "id", "value"] },
};

export const Indeterminate = Template.bind({});
Indeterminate.args = {
  label: "Label do checkbox",
  id: "ind-124",
  indeterminate: true,
  checked: true,
};
