/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { createLocalVue, shallowMount } from "@vue/test-utils";
import Tooltip from "./Tooltip.ce.vue";
import TooltipHeader from "./TooltipHeader.ce.vue";
import TooltipBody from "./TooltipBody.ce.vue";
import TooltipFooter from "./TooltipFooter.ce.vue";

const localVue = createLocalVue();
localVue.component("br-tooltip", Tooltip);
localVue.component("br-tooltip-header", TooltipHeader);
localVue.component("br-tooltip-body", TooltipBody);
localVue.component("br-tooltip-footer", TooltipFooter);

describe("br-tooltip", () => {
  const Component = {
    name: "br-tooltip",
    template: `
    <div>
      <input type="button" id="btn">Mostrar Tooltip</input>
      <slot name="tooltip"></slot>
    </div>
  `,
  };

  test("Testa se o uso da classe br-tooltip está sendo renderizado.", () => {
    const wrapper = shallowMount(Component, {
      localVue,
      slots: {
        tooltip: `
          <br-tooltip text="Texto do tooltip"></br-tooltip>
        `,
      },
    });
    expect(wrapper.find(".br-tooltip").exists()).toBe(true);
  });

  test("Testa se o br-tooltip renderizando com active=true.", () => {
    const wrapper = shallowMount(Component, {
      localVue,
      slots: {
        tooltip: `
          <br-tooltip active text="Texto do tooltip"></br-tooltip>
        `,
      },
    });
    expect(wrapper.find(".br-tooltip").attributes("active")).toBe("true");
  });

  test("Testa se o br-tooltip renderizando com timer.", () => {
    const wrapper = shallowMount(Component, {
      localVue,
      slots: {
        tooltip: `
          <br-tooltip timer="4000" text="Texto do tooltip"></br-tooltip>
        `,
      },
    });
    expect(wrapper.find(".br-tooltip").attributes("timer")).toBe("4000");
  });

  test("Testa se o br-tooltip renderizando com place=top.", () => {
    const wrapper = shallowMount(Component, {
      localVue,
      slots: {
        tooltip: `
          <br-tooltip place="top" text="Texto do tooltip"></br-tooltip>
        `,
      },
    });
    expect(wrapper.find(".br-tooltip").attributes("place")).toBe("top");
  });

  test("Testa se o tooltip do success foi renderizado.", () => {
    const wrapper = shallowMount(Component, {
      localVue,
      slots: {
        tooltip: `
          <br-tooltip text="Texto do tooltip" success="success"></br-tooltip>
        `,
      },
    });
    expect(wrapper.find(".br-tooltip").attributes("success")).toBe("true");
  });

  test("Testa se o tooltip do warning foi renderizado.", () => {
    const wrapper = shallowMount(Component, {
      localVue,
      slots: {
        tooltip: `
          <br-tooltip text="Texto do tooltip" warning="warning"></br-tooltip>
        `,
      },
    });
    expect(wrapper.find(".br-tooltip").attributes("warning")).toBe("true");
  });

  test("Testa se o tooltip do info foi renderizado.", () => {
    const wrapper = shallowMount(Component, {
      localVue,
      slots: {
        tooltip: `
          <br-tooltip text="Texto do tooltip" info="info"></br-tooltip>
        `,
      },
    });
    expect(wrapper.find(".br-tooltip").attributes("info")).toBe("true");
  });

  test("Testa se o tooltip do error foi renderizado.", () => {
    const wrapper = shallowMount(Component, {
      localVue,
      slots: {
        tooltip: `
          <br-tooltip text="Texto do tooltip" error="error"></br-tooltip>
        `,
      },
    });
    expect(wrapper.find(".br-tooltip").attributes("error")).toBe("true");
  });

  test("Testa se o tooltip popover foi renderizado.", () => {
    const wrapper = shallowMount(Component, {
      localVue,
      slots: {
        tooltip: `
          <br-tooltip text="Texto do tooltip" popover>
            <br-tooltip-body slot="body">
              <div>
                <p>Conteúdo do popover</p>
              </div>
            </br-tooltip-body>
          </br-tooltip>
        `,
      },
    });
    expect(wrapper.find(".br-tooltip").attributes("popover")).toBe("true");
  });

  test("Testa se o popover foi renderizado com header.", () => {
    const wrapper = shallowMount(Component, {
      localVue,
      slots: {
        tooltip: `
          <br-tooltip text="Texto do tooltip" popover>
            <br-tooltip-body slot="body">
              <div>
                <p>Conteúdo do popover</p>
              </div>
            </br-tooltip-body>
            <br-tooltip-header slot="header">
              <div>
                <p>Header do popover</p>
              </div>
            </br-tooltip-header>
          </br-tooltip>
        `,
      },
    });
    expect(wrapper.find(".popover-header").exists()).toBe(true);
  });

  test("Testa se o popover foi renderizado com footer.", () => {
    const wrapper = shallowMount(Component, {
      localVue,
      slots: {
        tooltip: `
          <br-tooltip text="Texto do tooltip" popover>
            <br-tooltip-body slot="body">
              <div>
                <p>Conteúdo do popover</p>
              </div>
            </br-tooltip-body>
            <br-tooltip-footer slot="footer">
              <div>
                <p>Footer do popover</p>
              </div>
            </br-tooltip-footer>
          </br-tooltip>
        `,
      },
    });
    expect(wrapper.find(".popover-footer").exists()).toBe(true);
  });

  test("Testa se o popover foi renderizado com erro por falta do slot body.", () => {
    spyOn(console, "error");

    const wrapper = shallowMount(Component, {
      localVue,
      slots: {
        tooltip: `
          <br-tooltip text="Texto do tooltip" popover>            
          </br-tooltip>
        `,
      },
    });
    expect(console.error).toHaveBeenCalled();
  });
});
