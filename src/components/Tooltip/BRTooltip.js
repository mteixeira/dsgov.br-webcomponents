/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { createPopper } from "@popperjs/core";
class BRTooltip {
  constructor(name, component, activator) {
    this.name = name;
    this.component = component;
    this.activator = activator;
    const place = component.getAttribute("place");
    const positions = ["top", "right", "bottom", "left"];
    this.popover = component.hasAttribute("popover");
    this.notification = component.classList.contains("br-notification");
    this.timer = component.getAttribute("timer");
    this.active = component.hasAttribute("active");
    this.placement = positions.includes(place)
      ? place
      : this.notification
      ? "bottom"
      : "top";
    this.popperInstance = null;
    this.showEvents = ["mouseenter", "click", "focus"];
    this.hideEvents = ["mouseleave", "blur"];
    this.closeTimer = null;
    this._create();
    this._setBehavior();
  }

  forceUpdate() {
    this.popperInstance.forceUpdate();
  }

  _setBehavior() {
    // Ação de abrir padrao ao entrar no ativador
    if (this.activator) {
      this.showEvents.forEach((event) => {
        this.activator.addEventListener(event, (otherEvent) => {
          this._show(otherEvent);
        });
      });
    }
    // Adiciona ação de fechar ao botao do popover
    // if (this.popover || this.notification) {
    if (this.popover) {
      const closeBtn = this.component.querySelector(".close");
      closeBtn.addEventListener("click", (event) => {
        this._hide(event, this.component);
        this._toggleActivatorIcon();
      });
      // Ação de fechar padrao ao sair do ativador
    } else {
      this.hideEvents.forEach((event) => {
        this.activator.addEventListener(event, (otherEvent) => {
          this._hide(otherEvent, this.component);
        });
      });
    }
  }

  _create() {
    this._setLayout();
    // Cria a instancia do popper
    if (this.notification) {
      this.component.setAttribute("notification", "");
      this.popperInstance = createPopper(this.activator, this.component, {
        modifiers: [
          {
            name: "offset",
            options: {
              offset: [0, 10],
            },
          },
          {
            name: "preventOverflow",
            options: {
              altAxis: false, // false by default
              mainAxis: true, // true by default
              // rootBoundary: 'body',
            },
          },
        ],
        // placement: this.placement,
        placement: "bottom",
        strategy: "fixed",
      });
    } else {
      const ac = this.activator.getBoundingClientRect();
      const tt = this.component.getBoundingClientRect();
      const bw = document.body.clientWidth;

      if (this.placement === "right") {
        this.placement =
          ac.x + ac.width + tt.width > bw ? "top" : this.placement;
      }
      if (this.placement === "left") {
        this.placement = ac.x - tt.width > 0 ? this.placement : "top";
      }
      this.popperInstance = createPopper(this.activator, this.component, {
        modifiers: [
          {
            name: "offset",
            options: {
              offset: [0, 8],
            },
          },
          {
            name: "preventOverflow",
            options: {
              altAxis: true, // false by default
              // boundary: 'body',
              mainAxis: true, // true by default
              // rootBoundary: 'document',
              tether: false, // true by default
            },
          },
        ],
        placement: this.placement,
      });
    }
  }

  _show(event) {
    this.popperInstance.forceUpdate();
    this.component.style.display = "unset";
    this.component.setAttribute("data-show", "");
    this.component.style.zIndex = 99;
    this._fixPosition();
    // Importante pois "display: none" conflitua com a instancia do componente e precisa ser setado aqui já que pelo css ativa o efeito fade no primeiro carregamento
    this.component.style.visibility = "visible";
    if (this.timer) {
      clearTimeout(this.closeTimer);
      this.closeTimer = setTimeout(
        this._hide,
        this.timer,
        event,
        this.component
      );
    }
  }

  _hide(event, component) {
    component.removeAttribute("data-show");
    component.style.zIndex = -1;
    component.style.visibility = "hidden";
    clearTimeout(component.closeTimer);
  }

  _setLayout() {
    // Cria a setinha que aponta para o item que criou o tooltip
    const arrow = document.createElement("div");
    arrow.setAttribute("data-popper-arrow", "");
    arrow.classList.add("arrow");
    this.component.appendChild(arrow);
  }

  _toggleActivatorIcon() {
    const icon = this.activator.querySelector("button svg");
    if (icon) {
      icon.classList.toggle("fa-angle-down");
      icon.classList.toggle("fa-angle-up");
    }
    this.activator.toggleAttribute("active");
  }

  _fixPosition() {
    if (this.notification) {
      setTimeout(() => {
        const ac = this.activator.getBoundingClientRect();
        this.component.style = `position: fixed !important; top: ${
          ac.top + ac.height + 10
        }px !important; left: auto; right: 8px; display: unset; bottom: auto;`;
        this.component.querySelector(
          ".arrow"
        ).style = `position: absolute; left: auto; right: ${
          document.body.clientWidth - ac.right + ac.width / 5
        }px !important;`;
      }, 10);
    }
  }
}

export default BRTooltip;
