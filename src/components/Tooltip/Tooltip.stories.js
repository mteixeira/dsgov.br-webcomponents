/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrTooltip from "./Tooltip.ce.vue";
import { kebabiseArgs } from "../../util/Utils.js";

const slotHeader = `
  <br-tooltip-header slot="header">
    <span class="text">Cadastro Concluído!</span>
  </br-tooltip-header>`;

const slotBody = `
  <br-tooltip-body slot="body">
    <h1>Aqui pode estar um título da página</h1>
  </br-tooltip-body>`;

const slotFooter = `
  <br-tooltip-footer slot="footer">
    <a class="link" href="javascript:void(0)">Clique aqui</a>
  </br-tooltip-footer>`;

export default {
  title: "DSGOV/br-tooltip",
  component: BrTooltip,
  argTypes: {
    text: {
      defaultValue: "",
    },
    success: {
      defaultValue: false,
    },
    info: {
      defaultValue: false,
    },
    warning: {
      defaultValue: false,
    },
    error: {
      defaultValue: false,
    },
    place: {
      control: {
        type: "select",
        options: ["top", "bottom", "left", "right"],
      },
      defaultValue: "right",
    },
    header: {
      control: "text",
      description:
        "** header slot - corresponde ao título do tooltip do tipo popover. ",
      //defaultValue: ''
    },
    body: {
      control: "text",
      description:
        "** body slot - corresponde ao conteúdo do tooltip do tipo popover. Apresentar o texto informativo como padrão, mas pode-se utilizar recursos interativos como botões, links ou elementos gráficos, como imagens e ícones. ",
      //defaultValue: ''
    },
    footer: {
      control: "text",
      description:
        "** footer slot - corresponde ao footer do tooltip do tipo popover.",
      //defaultValue: ''
    },
  },
};

const TemplateComSlot = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-button label="Abrir Tooltip" type="secondary" class="mt-5" id="tooltip-activator-02"></br-button>
<br-tooltip v-bind="args">${args.header}${args.body}${args.footer}
</br-tooltip>
`,
});
/*
<div>
  <br-button label="Abrir Tooltip" type="secondary" class="mt-5" id="tooltip-activator-02"></br-button>
  <br-tooltip popover success place="bottom" timer="4000" activator-id="tooltip-activator-02">
    <br-tooltip-header slot="header">
      <span class="text">Cadastro Concluído!</span>
    </br-tooltip-header>
    <br-tooltip-body slot="body">
      <h1>Aqui pode estar um título da página</h1>
    </br-tooltip-body>
    <br-tooltip-footer slot="footer">
      <a class="link" href="javascript:void(0)">Clique aqui</a>
    </br-tooltip-footer>
  </br-tooltip>
</div>
*/
export const Popover = TemplateComSlot.bind({});

Popover.args = {
  popover: true,
  success: true,
  place: "bottom",
  timer: "4000",
  header: slotHeader,
  body: slotBody,
  footer: slotFooter,
  activatorId: "tooltip-activator-02",
};

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<div>
  <a
    href="https://dsgov.estaleiro.serpro.gov.br/components/tooltip?tab=desenvolvedor"
    target="_blank"
  >
    Passe o mouse para ativar o tooltip
  </a>
  <br-tooltip v-bind="args"></br-tooltip>
</div>
`,
});

export const Default = Template.bind({});

Default.args = {
  popover: false,
  text: `Aqui o texto do tooltip`,
};
Default.parameters = {
  controls: { exclude: ["popover", "timer"] },
};
