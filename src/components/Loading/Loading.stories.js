/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrLoading from "./Loading.ce.vue";
import { kebabiseArgs } from "../../util/Utils.js";

export default {
  title: "Dsgov/br-loading",
  component: BrLoading,
  argTypes: {
    percent: {
      control: {
        type: "range",
        min: 0,
        max: 100,
        step: 1,
      },
    },
  },
};

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-loading v-bind="args"></br-loading>`,
});

export const Progress = Template.bind({});
Progress.args = {
  progress: true,
  percent: 10,
};

export const Indeterminate = Template.bind({});
Indeterminate.args = {
  rotulo: "Carregando...",
};

export const IndeterminateMedium = Template.bind({});
IndeterminateMedium.args = {
  rotulo: "Carregando...",
  medium: true,
};
