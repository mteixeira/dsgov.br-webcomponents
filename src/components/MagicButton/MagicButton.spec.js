/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount, createLocalVue } from "@vue/test-utils";
import BrMagicButton from "./MagicButton.ce.vue";

const localVue = createLocalVue();
localVue.component("br-magic-button", BrMagicButton);

const densidades = ["large", "medium", "small"];

describe("MagicButton", () => {
  test("it renders br-magic-button", () => {
    const wrapper = shallowMount(BrMagicButton);
    expect(wrapper.classes("br-magic-button")).toBe(true);
  });

  test("set label attribute to create pill type", () => {
    const rotulo = "Label";
    const wrapper = shallowMount(BrMagicButton, {
      localVue,
      propsData: {
        label: rotulo,
      },
    });
    expect(wrapper.text()).toMatch(rotulo);
  });

  test("set icon to create circle type", () => {
    const wrapper = shallowMount(BrMagicButton, {
      localVue,
      propsData: {
        icon: "fa-car",
      },
    });
    expect(wrapper.find(".br-button.circle").exists()).toBe(true);
  });

  densidades.forEach((densidade) => {
    test(`set density attribute ${densidade}`, () => {
      const rotulo = "Texto do Botão";
      const wrapper = shallowMount(BrMagicButton, {
        localVue,
        propsData: {
          density: densidade,
          label: rotulo,
        },
      });
      expect(wrapper.find(`.br-magic-button.${densidade}`).exists()).toBe(true);
      // expect(wrapper.element).toMatchSnapshot()
    });
  });
});
