/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import Button from "./Button.ce.vue";
import { kebabiseArgs } from "../../util/Utils.js";

export default {
  title: "Dsgov/br-button",
  component: Button,
  parameters: {
    backgrounds: {
      default: "light",
      values: [
        { name: "light", value: "#fff" },
        { name: "dark", value: "#071D41" },
      ],
    },
  },
  argTypes: {
    label: {
      control: "text",
    },
    type: {
      control: {
        type: "select",
        options: ["secondary", "primary"],
      },
    },
    density: {
      control: {
        type: "select",
        options: ["large", "middle", "small", "xsmall"],
      },
    },
  },
};
const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-button v-bind="args"></br-button>`,
});

export const Primary = Template.bind({});
Primary.args = {
  label: "Primary",
  type: "primary",
};

export const Secondary = Template.bind({});
Secondary.args = {
  label: "Secondary",
  type: "secondary",
};

export const Tertiary = Template.bind({});
Tertiary.args = {
  label: "Tertiary",
  inverted: false,
};

export const Circle = Template.bind({});
Circle.parameters = { controls: { exclude: ["label", "block"] } };
Circle.args = {
  type: "primary",
  circle: true,
  icon: "trash",
  inverted: false,
};

export const Loading = Template.bind({});
Loading.parameters = {
  controls: { exclude: ["label", "block", "disabled", "icon", "circle"] },
};
Loading.args = {
  type: "secondary",
  loading: true,
};

export const Inverted = Template.bind({});
Inverted.parameters = { backgrounds: { default: "dark" } };
Inverted.args = {
  label: "Inverted",
  type: "secondary",
  inverted: true,
};

export const IconAndLabel = Template.bind({});
IconAndLabel.args = {
  label: "Text",
  type: "secondary",
  icon: "city",
  inverted: false,
};

export const IconAndCircle = Template.bind({});
IconAndCircle.parameters = { controls: { exclude: ["label", "block"] } };
IconAndCircle.args = {
  circle: true,
  type: "secondary",
  icon: "city",
  inverted: false,
};
