/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount, createLocalVue } from "@vue/test-utils";
import Button from "./Button.ce.vue";

const localVue = createLocalVue();
localVue.component("br-button", Button);

const densidades = ["large", "middle", "small"];
const tipo = ["secondary", "primary"];

describe("Button", () => {
  test("it renders br-button", () => {
    const wrapper = shallowMount(Button);
    expect(wrapper.classes("br-button")).toBe(true);
  });

  test("set label attribute", () => {
    const rotulo = "Label do button";
    const wrapper = shallowMount(Button, {
      localVue,
      propsData: {
        label: rotulo,
      },
    });
    expect(wrapper.text()).toMatch(rotulo);
  });

  test("set circle attribute", () => {
    const wrapper = shallowMount(Button, {
      localVue,
      propsData: {
        circle: true,
      },
    });
    expect(wrapper.classes("circle")).toBe(true);
  });

  test("assign inverted property to button and check if it is true", () => {
    const wrapper = shallowMount(Button, {
      localVue,
      propsData: {
        inverted: true,
      },
    });
    expect(wrapper.find(".br-button.inverted").exists()).toBe(true);
  });

  test("set disabled attribute", () => {
    const wrapper = shallowMount(Button, {
      localVue,
      propsData: {
        disabled: true,
      },
    });
    expect(wrapper.find(".br-button").props().disabled).toBe(true);
  });

  test("set block attribute", () => {
    const wrapper = shallowMount(Button, {
      localVue,
      propsData: {
        block: true,
      },
    });
    expect(wrapper.classes("block")).toBe(true);
  });

  densidades.forEach((densidade) => {
    test(`set density attribute ${densidade}`, () => {
      const wrapper = shallowMount(Button, {
        localVue,
        propsData: {
          density: densidade,
        },
      });
      expect(wrapper.find(`.${densidade}`).exists()).toBe(true);
      expect(wrapper.element).toMatchSnapshot();
    });
  });

  tipo.forEach((cor) => {
    test(`set type attribute ${cor}`, () => {
      const wrapper = shallowMount(Button, {
        localVue,
        propsData: {
          type: cor,
        },
      });
      expect(wrapper.find(`.${cor}`).exists()).toBe(true);
      expect(wrapper.element).toMatchSnapshot();
    });
  });
});
