/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount } from "@vue/test-utils";
import BrList from "./List.ce.vue";

describe("br-list", () => {
  test("Testa se a classe br-list está renderizando.", () => {
    const wrapper = shallowMount(BrList);
    expect(wrapper.find(".br-list").exists()).toBe(true);
  });

  test("Testa se a prop horizontal está renderizando a classe respectiva.", () => {
    const wrapper = shallowMount(BrList, {
      propsData: { horizontal: true },
    });
    expect(wrapper.find(".horizontal").exists()).toBe(true);
  });

  test("Testa se a prop title é exibida.", () => {
    const titleExample = "title example";
    const wrapper = shallowMount(BrList, {
      propsData: {
        title: titleExample,
      },
    });
    expect(wrapper.text()).toMatch(titleExample);
  });
});
