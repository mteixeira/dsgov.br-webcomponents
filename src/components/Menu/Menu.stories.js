/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrMenu from "./Menu.ce.vue";
import { kebabiseArgs } from "../../util/Utils.js";

export default {
  title: "Dsgov/br-menu",
  component: BrMenu,
  argTypes: {
    list: {
      control: "text",
    },
  },
};

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-menu v-bind="args"></br-menu>`,
});

export const completo = Template.bind({});
completo.args = {
  list: `
  {
    'menu group 1': [
      'menu item 1',
      {
        'text': 'menu item ',
        'list': [
          'submenu item 1',
          'submenu item 1',
          'submenu item 1'
        ]
      },
      'menu item'
    ],
    'menu group 2': [
      'menu item 2',
      {
        'text': 'menu item ',
        'list': [
          'submenu item 2',
          {
            'text': 'submenu item 2',
            'list': [
              'child submenu item 2',
              {
                'text': 'child submenu item 2',
                'list': [
                  'last level',
                  'last level',
                  'last level'
                ]
              },
              'child submenu item 2'
            ]  
          },
          'submenu item 2'
        ]
      },
      'menu item'
    ]
  }`,
};
