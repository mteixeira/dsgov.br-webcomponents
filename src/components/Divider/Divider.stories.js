/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrDivider from "./Divider.ce.vue";
import { kebabiseArgs } from "../../util/Utils.js";

export default {
  title: "Dsgov/br-divider",
  component: BrDivider,
  argTypes: {
    vertical: {
      control: "boolean",
      default: false,
    },
  },
};

const TemplateVertical = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<div class="row m-5">
  <div class="col sm p-3">
    <div class="d-flex">
      <p>
        Voluptate ipsa iure placeat praesentium, sint deleniti consectetur
        quisquam neque veniam possimus, hic commodi?
      </p>
      <br-divider v-bind="args" class='d-flex mx-3'></br-divider>
      <p>
        A adipisci mollitia blanditiis itaque velit laudantium voluptatum
        molestiae quasi.
      </p>
    </div>
    <br />
    <div class="bg-secondary-07 text-secondary-01 p-3">
      <div class="d-flex">
        <p>
          Voluptate ipsa iure placeat praesentium, sint deleniti consectetur
          quisquam neque veniam possimus, hic commodi?
        </p>
        <br-divider v-bind="args" class='d-flex mx-3'></br-divider>
        <p>
          A adipisci mollitia blanditiis itaque velit laudantium voluptatum
          molestiae quasi.
        </p>
      </div>
    </div>
  </div>
</div>
  `,
});

export const Vertical = TemplateVertical.bind({});
Vertical.args = {
  vertical: true,
};

const TemplateDefault = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<div class="row m-5">
  <div class="col sm p-3">
    <div>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
      <br-divider v-bind="args"></br-divider>
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
    </div>
  </div>
</div>
  `,
});

export const Default = TemplateDefault.bind({});
Default.args = {};
Default.parameters = {
  controls: { exclude: ["vertical"] },
};

const TemplateHorizontal = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<div class="row m-5">
  <div class="col sm p-3">
    <div>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
      <br-divider v-bind="args"></br-divider>
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
    </div>
    <br />
    <div class="bg-secondary-07 text-secondary-01 p-3">
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
      <br-divider v-bind="args"></br-divider>
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
    </div>
  </div>
</div>
  `,
});

export const Horizontal = TemplateHorizontal.bind({});
Horizontal.args = {};

Horizontal.parameters = {
  controls: { exclude: ["vertical"] },
};
