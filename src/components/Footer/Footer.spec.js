/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount, createLocalVue } from "@vue/test-utils";
import BrFooter from "./Footer.ce.vue";
import BrList from "../List/List.ce.vue";
import BrItem from "../Item/Item.ce.vue";
import BrFooterLogo from "../FooterLogo/FooterLogo.ce.vue";
import BrFooterImage from "../FooterImage/FooterImage.ce.vue";
import BrFooterSocialNetwork from "../FooterSocialNetwork/FooterSocialNetwork.ce.vue";
import BrFooterSocialNetworkItem from "../FooterSocialNetworkItem/FooterSocialNetworkItem.ce.vue";

const localVue = createLocalVue();
localVue.component("br-list", BrList);
localVue.component("br-item", BrItem);
localVue.component("br-footer-social-network", BrFooterSocialNetwork);
localVue.component("br-footer-social-network-item", BrFooterSocialNetworkItem);
localVue.component("br-footer-logo", BrFooterLogo);
localVue.component("br-footer-image", BrFooterImage);

describe("BrFooter", () => {
  test("Testa se a prop text está sendo renderizada.", () => {
    const textHello = "hello";
    const wrapper = shallowMount(BrFooter, {
      localVue,
      propsData: { text: textHello },
    });
    expect(wrapper.text()).toMatch(textHello);
  });

  test("Testa se a prop inverted está renderizando o uso da classe CSS inverted.", () => {
    const wrapper = shallowMount(BrFooter, {
      localVue,
      propsData: { inverted: true },
    });
    expect(wrapper.find(".inverted").exists()).toBe(true);
  });

  test("Testa a renderização das listas de links.", () => {
    const wrapper = shallowMount(BrFooter, {
      localVue,
      slots: {
        categorias: `
        <br-list horizontal dataToggle data-unique>
        <br-item header title="CATEGORIA NUMERO UM" />
           <br-list>
             <br-item href="#" hover>link 1</br-item>
           </br-list>
       
        <br-item header title="CATEGORIA NÚMERO DOIS" />
          <br-list>
           <br-item href="#" hover>link 2</br-item>
           <br-item href="#" hover>link 3</br-item>
          </br-list>
        
        <br-item header title="CATEGORIA NÚMERO TRES" />
          <br-list>
              <br-item href="#" hover>link 4</br-item>
              <br-item href="#" hover>link 5</br-item>
          </br-list>
        
        <br-item header title="CATEGORIA NÚMERO QUATRO" />
         <br-list>
            <br-item href="#" hover>link 6</br-item>
            <br-item href="#" hover>link 7</br-item>
          </br-list>
        
        <br-item header title="CATEGORIA NÚMERO CINCO" />
          <br-list>
           <br-item href="#" hover>link 8</br-item>
            <br-item href="#" hover>link 9</br-item>
          </br-list>
        
        <br-item header title="CATEGORIA NÚMERO SEIS" />
         <br-list>
            <br-item href="#" hover>link 10</br-item>
            <br-item href="#" hover>link 11</br-item>
          </br-list>
          </br-list>
        `,
        "slot-não-existente": `
        <br-item header title="Categoria Número Sete" />  
         <br-list>
            <br-item href="#" hover>link 12</br-item>
            <br-item href="#" hover>link 13</br-item>
          </br-list>
          
        `,
      },
    });
    expect(wrapper.findAllComponents(BrList)).toHaveLength(7);
    expect(wrapper.findAllComponents(BrItem)).toHaveLength(17);
  });

  test("Testa a renderização dos componentes de redes sociais.", () => {
    const label = "Redes Sociais";
    const wrapper = shallowMount(BrFooter, {
      localVue,
      slots: {
        redesSociais: `
        <br-footer-social-network slot="redes-sociais" label="${label}">
          <br-footer-social-network-item url="#facebook">
            <span class="fab fa-facebook"></span>
          </br-footer-social-network-item>
          <br-footer-social-network-item url="#twitter">
            <span class="fab fa-twitter"></span>
          </br-footer-social-network-item>
          <br-footer-social-network-item url="#instagram">
            <span class="fab fa-instagram"></span>
          </br-footer-social-network-item>
          <br-footer-social-network-item url="#linkedin">
            <span class="fab fa-linkedin"></span>
          </br-footer-social-network-item>
        </br-footer-social-network>
      </br-footer>
        `,
      },
    });
    expect(wrapper.find(".social-network").exists()).toBe(true);
    expect(wrapper.text()).toMatch(label);
    expect(wrapper.findAllComponents(BrFooterSocialNetworkItem)).toHaveLength(
      4
    );
  });
  /*****/
  test("Testa se a logo  está renderizando", () => {
    const varURL = "https://url-de-teste";
    const wrapper = shallowMount(BrFooter, {
      localVue,
      slots: {
        logo: `
        <br-footer-logo src="https://url-de-teste" 
        alt="Acesso à informação" 
        slot="logo"></br-footer-logo>`,
      },
    });
    expect(wrapper.find("img[src='" + varURL + "']").exists()).toBe(true);
  });

  test("Testa se as images direita do rodapé estão renderizando", () => {
    const srcImg = "https://url-de-teste";
    const srcImg2 = "https://url-de-teste2";
    const wrapper = shallowMount(BrFooter, {
      localVue,
      slots: {
        footerImagens: `<br-footer-image src="https://url-de-teste" 
        alt="Acesso à informação" 
        href="#acesso-a-informacao1" 
        slot="footerImagens"></br-footer-image>
      <br-footer-image 
        src="https://url-de-teste2" 
        alt="Acesso à informação" 
        href="#acesso-a-informacao2" 
        slot="footerImagens"></br-footer-image>`,
      },
    });
    expect(wrapper.find("img[src='" + srcImg + "']").exists()).toBe(true);
    expect(wrapper.find("img[src='" + srcImg2 + "']").exists()).toBe(true);
  });
});
