/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrFooter from "./Footer.ce.vue";
import { kebabiseArgs } from "../../util/Utils.js";

const slotCategorias = `
  <br-list horizontal="true" data-toggle="true" data-unique="true" slot="categorias"> 
    <div class="col-2">
      <br-item header="true" title="SERVICOS"/>
      <br-list>
        <br-item href="#">Financas, Impostos e gestao Publica</br-item>
        <br-item href="#">Assistencia Social</br-item>
        <br-item href="#">Justiça e Segurança</br-item>
      </br-list>
    </div>

    <div class="col-2">
    <br-item header="true" title="GALERIA DE APLICATIVOS" />
      <br-list> 
        <br-item href="#">Trabalho e previdencia</br-item>
        <br-item href="#">Energias, minerais e combustiveis</br-item>
        <br-item href="#">Justiça e Seguraça</br-item>
        <br-item href="#">Educação e pesquisa</br-item>
      </br-list>
    </div>

    <div class="col-2">
    <br-item header="true" title="NOTICIAS" />
      <br-list>
        <br-item href="#">Assistencia Social</br-item>
        <br-item href="#">Meio ambiente e clima</br-item>
        <br-item href="#">Viagens e Turismo</br-item>
        <br-item href="#">Agricultura e pecuaria</br-item>
      </br-list>
    </div>

    <div class="col-2">
    <br-item header="true" title="PLANALTO" />
      <br-list>
        <br-item href="#">Agricultura e pecuaria</br-item>
        <br-item href="#">Transito e transportes</br-item>
        <br-item href="#">Assistencia Social</br-item>
        <br-item href="#">Cultura, artes, historia e esportes</br-item>
      </br-list>
    </div>

    <div class="col-2">
    <br-item header="true" title="CANAIS DO PODER EXECUTIVO FEDERAL" />
      <br-list>   
        <br-item href="#">Cultura, artes, historia e esportes</br-item>
        <br-item href="#">Financas, Impostos e gestao Publica</br-item>
        <br-item href="#">Viagens e Turismo</br-item>
      </br-list>
    </div>
  </br-list>`;

const slotSocialNetwork = `
  <br-footer-social-network label="Redes Sociais" slot="redesSociais">
    <br-footer-social-network-item url="http://www.facebook.com">
      <span class="fab fa-facebook"></span>
    </br-footer-social-network-item>
    <br-footer-social-network-item url="http://www.twitter.com">
      <span class="fab fa-twitter"></span>
    </br-footer-social-network-item>
    <br-footer-social-network-item url="http://www.instagram.com">
      <span class="fab fa-instagram"></span>
    </br-footer-social-network-item>
    <br-footer-social-network-item url="http://www.linkedin.com">
      <span class="fab fa-linkedin"></span>
    </br-footer-social-network-item>
  </br-footer-social-network>
      `;
const slotLogo = `
  <br-footer-logo src="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-negative.png" 
    alt="Acesso à informação" 
    slot="logo" centered="false"></br-footer-logo>`;

const slotLogoVazio = `
  <br-footer-logo slot="logo"></br-footer-logo>`;

const slotLogoInverted = `
  <br-footer-logo src="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-negative.png" 
    alt="Acesso à informação" 
    slot="logo" inverted="true"></br-footer-logo>`;

const slotImagensInverted = `
  <br-footer-image src="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-assign-negative.png" 
    alt="Acesso à informação" 
    href="#acesso-a-informacao1" 
    slot="footerImagens" inverted="true"></br-footer-image>
  <br-footer-image 
    src="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-assign-negative.png" 
    alt="Acesso à informação" 
    href="#acesso-a-informacao2" 
    slot="footerImagens" inverted="true"></br-footer-image>
`;

const slotImagens = `
  <br-footer-image src="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-assign-negative.png" 
    alt="Acesso à informação" 
    href="#acesso-a-informacao1" 
    slot="footerImagens"></br-footer-image>
  <br-footer-image 
    src="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-assign-negative.png" 
    alt="Acesso à informação" 
    href="#acesso-a-informacao2" 
    slot="footerImagens"></br-footer-image>
`;

export default {
  title: "Dsgov/br-footer",
  component: BrFooter,
  argTypes: {
    inverted: {
      control: "boolean",
    },
    text: {
      control: "text",
    },
    categorias: {
      description:
        "**[OBRIGATÓRIO]** Conteudo das categorias a serem exibidas no footer, que deve ser passado por slot",
      control: "text",
      type: {
        required: true,
      },
    },
    redesSociais: {
      description:
        "**[OPCIONAL]** Links das redes sociais, que pode ser passado por slot.",
      control: "text",
      type: {
        required: true,
      },
    },
    logo: {
      description:
        "**[OBRIGATÓRIO]** Conteúdo da logo, que deve ser passado por slot.",
      control: "text",
      type: {
        required: true,
      },
    },
    footerImagens: {
      description:
        "**[OPCIONAL]** Imagens do rodapé, que pode ser passado por slot.",
      control: "text",
      type: {
        required: true,
      },
    },
  },
};

const TemplateCompleto = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-footer v-bind="args">
  ${args.categorias}
  ${args.redesSociais}
  ${args.logo}
  ${args.footerImagens}
</br-footer>
  `,
});

export const Completo = TemplateCompleto.bind({});
Completo.args = {
  inverted: false,
  text: "Todo o conteúdo deste site está publicado sob a licença Creative Commons",
  logo: slotLogo,
  footerImagens: slotImagens,
  categorias: slotCategorias,
  redesSociais: slotSocialNetwork,
};

export const Inverted = TemplateCompleto.bind({});
Inverted.args = {
  inverted: true,
  text: "Todo o conteúdo deste site está publicado sob a licença Creative Commons",
  logo: slotLogoInverted,
  footerImagens: slotImagensInverted,
  categorias: slotCategorias,
  redesSociais: slotSocialNetwork,
};

const TemplateApenasLinks = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-footer v-bind="args">
  ${args.categorias}
  ${args.logo}
</br-footer>
`,
});

export const ApenasLinks = TemplateApenasLinks.bind({});
ApenasLinks.args = {
  categorias: slotCategorias,
  logo: slotLogoVazio,
};
