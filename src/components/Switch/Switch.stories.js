/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrSwitch from "./Switch.ce.vue";
import { kebabiseArgs } from "../../util/Utils.js";

const slotDefault = `
  <img src="https://dsgov.estaleiro.serpro.gov.br/assets/img/govbr-logo-large.png"
    title="Imagem passada por slot" alt="DSGov" />
`;
export default {
  title: "Dsgov/br-switch",
  component: BrSwitch,
  argTypes: {
    checked: {
      control: "boolean",
      description: "Determina se o switch está checado.",
    },
    disabled: {
      description: "Determina se o switch está desabilitado.",
    },
    icon: {
      control: "boolean",
      description:
        "Determina se o switch possui ícones representativos para o checked.",
    },
    size: {
      control: { type: "select", options: ["small", "medium", "large"] },
      description: "Opções de tamanho do componente (small, medium e large).",
    },
    label: {
      control: "text",
      description: "Rótulo do componente.",
    },
    default: {
      control: "text",
      description:
        'Rótulo do componente passado como slot (prevalece sobre o atributo "label").',
    },
    labelChecked: {
      control: "text",
      description: "Rótulo para o checked true.",
    },
    labelNotChecked: {
      control: "text",
      description: "Rótulo para o checked false.",
    },
  },
};

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-switch v-bind="args"></br-switch>`,
});

const TemplateComSlot = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-switch v-bind="args">${args.default}</br-switch>`,
});

export const Completo = TemplateComSlot.bind({});
Completo.args = {
  icon: true,
  checked: true,
  size: "large",
  labelChecked: "Sucesso",
  labelNotChecked: "Erro",
  default: slotDefault,
};

export const Simples = Template.bind({});
Simples.args = {
  label: "Simples",
};
