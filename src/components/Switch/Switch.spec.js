/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount } from "@vue/test-utils";
import Switch from "./Switch.ce.vue";

jest.spyOn(global.console, "error").mockImplementation((s) => {
  throw s;
});

describe("br-switch", () => {
  test("Testa se o uso da classe br-switch está sendo renderizado.", () => {
    const wrapper = shallowMount(Switch, {
      propsData: {
        id: "123",
      },
    });
    expect(wrapper.find(".br-switch").exists()).toBeTruthy();
  });

  test("Testa se a prop checked está sendo renderizada.", () => {
    const wrapper = shallowMount(Switch, {
      propsData: {
        id: "123",
        checked: true,
      },
    });
    expect(wrapper.find("input").element.checked).toBeTruthy();
  });

  test("Testa se a prop disabled está sendo renderizada.", () => {
    const wrapper = shallowMount(Switch, {
      propsData: {
        id: "123",
        disabled: true,
      },
    });
    expect(wrapper.find("input").element.disabled).toBeTruthy();
  });

  test("Testa se a prop icon está sendo renderizada.", () => {
    const wrapper = shallowMount(Switch, {
      propsData: {
        id: "123",
        icon: true,
      },
    });
    expect(wrapper.find(".br-switch.icon").exists()).toBeTruthy();
  });

  test("Testa se a prop size com valor small está sendo renderizada.", () => {
    const wrapper = shallowMount(Switch, {
      propsData: {
        id: "123",
        size: "small",
      },
    });
    expect(wrapper.find(".br-switch.small").exists()).toBeTruthy();
  });

  test("Testa se a prop size com valor large está sendo renderizada.", () => {
    const wrapper = shallowMount(Switch, {
      propsData: {
        id: "123",
        size: "large",
      },
    });
    expect(wrapper.find(".br-switch.large").exists()).toBeTruthy();
  });

  test("Testa se a prop label está sendo renderizada.", () => {
    const textoHello = "hello";
    const wrapper = shallowMount(Switch, {
      propsData: {
        id: "123",
        label: textoHello,
      },
    });
    expect(wrapper.text()).toMatch(textoHello);
  });

  test("Testa se as props labelChecked e labelNotChecked estão sendo renderizadas.", () => {
    const labelChecked = "ON";
    const labelNotChecked = "OFF";
    const wrapper = shallowMount(Switch, {
      propsData: {
        id: "123",
        labelChecked: labelChecked,
        labelNotChecked: labelNotChecked,
      },
    });
    expect(wrapper.html()).toMatch('data-enabled="' + labelChecked + '"');
    expect(wrapper.html()).toMatch('data-disabled="' + labelNotChecked + '"');
  });
});
