/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount } from "@vue/test-utils";
import BrHeader from "./Header.ce.vue";
import BrHeaderLogo from "./HeaderLogo.ce.vue";
import BrHeaderActionLink from "./HeaderActionLink";
import BrHeaderActionFunction from "./HeaderActionFunction";
import BrHeaderActionSearch from "./HeaderActionSearch";
import BrHeaderActionLogin from "./HeaderActionLogin";
import BrHeaderMenu from "./HeaderMenu";
import BrHeaderTitle from "./HeaderTitle";

describe("Header", () => {
  test("it renders Header component", () => {
    const wrapper = shallowMount(BrHeader);
    expect(wrapper.classes("br-header")).toBe(true);
  });

  test("it renders BrHeaderLogo", async () => {
    const link =
      "https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-positive.png";
    const assinatura = "Assinatura da Empresa";

    const wrapper = shallowMount(BrHeaderLogo, {
      propsData: {
        src: link,
        signature: assinatura,
      },
    });
    expect(wrapper.find(".header-logo").exists()).toBe(true);
    expect(wrapper.element).toMatchSnapshot();
  });

  test("it renders BrHeaderMenu", async () => {
    const wrapper = shallowMount(BrHeaderMenu);
    expect(wrapper.find(".header-menu-trigger").exists()).toBe(true);
    expect(wrapper.element).toMatchSnapshot();
  });

  test("it renders BrHeaderTitle", async () => {
    const link =
      "https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-positive.png";
    const assinatura = "Assinatura da Empresa";

    const wrapper = shallowMount(BrHeaderTitle, {
      propsData: {
        title: "Título do Header",
        subtitle: "Subtítulo do Header",
      },
    });
    expect(wrapper.find(".header-info").exists()).toBe(true);
    expect(wrapper.element).toMatchSnapshot();
  });

  test("it renders BrHeaderActionLink", async () => {
    const wrapper = shallowMount(BrHeaderActionLink, {
      propsData: {
        href: "https://www.serpro.gov.br/",
      },
      slots: {
        default: "<div>Título do link</div>",
      },
    });
    const linkClass = wrapper.find(".br-item");
    expect(linkClass.exists()).toBe(true);
    expect(linkClass.text().trim()).toBe("Título do link");
    expect(wrapper.element).toMatchSnapshot();
  });

  test("it renders BrHeaderActionFunction", async () => {
    const wrapper = shallowMount(BrHeaderActionFunction, {
      propsData: {
        icon: "fa-comment",
      },
    });
    expect(wrapper.find(".align-items-center.br-item").exists()).toBe(true);
    expect(wrapper.element).toMatchSnapshot();
  });

  test("it renders BrHeaderActionSearch", async () => {
    const wrapper = shallowMount(BrHeaderActionSearch);
    expect(wrapper.find(".header-search-trigger").exists()).toBe(true);
    expect(wrapper.element).toMatchSnapshot();
  });

  test("it renders BrHeaderActionLogin", async () => {
    const wrapper = shallowMount(BrHeaderActionLogin);
    expect(wrapper.find(".header-login").exists()).toBe(true);
    expect(wrapper.element).toMatchSnapshot();
  });
});
