/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrHeader from "./Header.ce.vue";
import { kebabiseArgs } from "../../util/Utils.js";

export default {
  title: "Dsgov/br-header",
  component: BrHeader,
};

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `
  <br-header v-bind="args">${args.slotTemplate}</br-header>
    `,
});

export const Complete = Template.bind({});
Complete.args = {
  slotTemplate: `
  <br-header-logo 
    slot="headerLogo" 
    src="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-positive.png" 
    signature="Assinatura da Empresa">
  </br-header-logo>
  <br-header-action slot="headerAction">
    <br-header-action-link slot="actionLink" >Home</br-header-action-link>
    <br-header-action-link slot="actionLink" href="#">Link 1</br-header-action-link>
    <br-header-action-link slot="actionLink" href="#">Link 2</br-header-action-link>
    <br-header-action-link slot="actionLink" href="#">Link 3</br-header-action-link>
    <br-header-action-function slot="actionFunction" icon="fa-chart-bar" text="Funcionalidade 1"></br-header-action-function>
    <br-header-action-function slot="actionFunction" icon="fa-comment" text="Funcionalidade 2"></br-header-action-function>
    <br-header-action-function slot="actionFunction" icon="fa-adjust" text="Funcionalidade 3"></br-header-action-function>
    <br-header-action-function slot="actionFunction" icon="fa-headset" text="Funcionalidade 4"></br-header-action-function>
    <br-header-action-login slot="actionLogin" label="Entrar" image="https://picsum.photos/id/823/400"></br-header-action-login>
  </br-header-action>
  <br-header-title slot="headerTitle" title="Título do Header" subtitle="Subtítulo do Header"></br-header-title>
  <br-header-menu slot="headerMenu"></br-header-menu>
  <br-header-search slot="headerSearch"></br-header-search>
  `,
};
Complete.parameters = { controls: { exclude: ["slotTemplate"] } };

export const Logo = Template.bind({});
Logo.args = {
  slotTemplate: `
  <br-header-logo 
      slot="headerLogo" 
      src="https://cdn.dsgovserprodesign.estaleiro.serpro.gov.br/design-system/images/logo-positive.png" 
      signature="Assinatura da Empresa">
  </br-header-logo>
  `,
};
Logo.parameters = { controls: { exclude: ["slotTemplate"] } };

export const Title = Template.bind({});
Title.args = {
  slotTemplate: `
  <br-header-title slot="headerTitle" title="Título do Header" subtitle="Subtítulo do Header"></br-header-title>
  `,
};
Title.parameters = { controls: { exclude: ["slotTemplate"] } };

export const Menu = Template.bind({});
Menu.args = {
  slotTemplate: `
  <br-header-menu slot="headerMenu"></br-header-menu>
  `,
};
Menu.parameters = { controls: { exclude: ["slotTemplate"] } };

export const Search = Template.bind({});
Search.args = {
  slotTemplate: `
  <br-header-search slot="headerSearch"></br-header-search>
  `,
};
Search.parameters = { controls: { exclude: ["slotTemplate"] } };

export const ActionLink = Template.bind({});
ActionLink.args = {
  slotTemplate: `
  <br-header-action slot="headerAction">
      <br-header-action-link slot="actionLink">Home</br-header-action-link>
      <br-header-action-link slot="actionLink">Link 1</br-header-action-link>
      <br-header-action-link slot="actionLink">Link 2</br-header-action-link>
      <br-header-action-link slot="actionLink">Link 3</br-header-action-link>
  </br-header-action>
  `,
};
ActionLink.parameters = { controls: { exclude: ["slotTemplate"] } };

export const ActionFunction = Template.bind({});
ActionFunction.args = {
  slotTemplate: `
  <br-header-action slot="headerAction">
      <br-header-action-function slot="actionFunction" icon="fa-chart-bar" text="Funcionalidade 1"></br-header-action-function>
      <br-header-action-function slot="actionFunction" icon="fa-comment" text="Funcionalidade 2"></br-header-action-function>
      <br-header-action-function slot="actionFunction" icon="fa-adjust" text="Funcionalidade 3"></br-header-action-function>
      <br-header-action-function slot="actionFunction" icon="fa-headset" text="Funcionalidade 4"></br-header-action-function>
  </br-header-action>
  `,
};
ActionFunction.parameters = { controls: { exclude: ["slotTemplate"] } };

export const ActionLinkAndFunction = Template.bind({});
ActionLinkAndFunction.args = {
  slotTemplate: `
  <br-header-action slot="headerAction">
    <br-header-action-link slot="actionLink" href="#">Home</br-header-action-link>
    <br-header-action-link slot="actionLink" href="#">Link 1</br-header-action-link>
    <br-header-action-link slot="actionLink" href="#">Link 2</br-header-action-link>
    <br-header-action-link slot="actionLink" href="#">Link 3</br-header-action-link>
    <br-header-action-function slot="actionFunction" icon="fa-chart-bar" text="Funcionalidade 1"></br-header-action-function>
    <br-header-action-function slot="actionFunction" icon="fa-comment" text="Funcionalidade 2"></br-header-action-function>
    <br-header-action-function slot="actionFunction" icon="fa-adjust" text="Funcionalidade 3"></br-header-action-function>
    <br-header-action-function slot="actionFunction" icon="fa-headset" text="Funcionalidade 4"></br-header-action-function>
  </br-header-action>
  `,
};
ActionLinkAndFunction.parameters = { controls: { exclude: ["slotTemplate"] } };

export const ActionLogin = Template.bind({});
ActionLogin.args = {
  slotTemplate: `
  <br-header-action slot="headerAction">
    <br-header-action-login slot="actionLogin" label="Entrar" image="https://picsum.photos/id/823/400">
    </br-header-action-login>
  </br-header-action>
  `,
};
ActionLogin.parameters = { controls: { exclude: ["slotTemplate"] } };
