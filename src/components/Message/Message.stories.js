/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrMessage from "./Message.ce.vue";
import { kebabiseArgs } from "../../util/Utils.js";

const slotDefault = `Texto da mensagem.`;

export default {
  title: "Dsgov/br-message",
  component: BrMessage,
  argTypes: {
    state: {
      control: {
        type: "select",
        options: ["info", "warning", "danger", "success"],
      },
      defaultValue: "",
    },
    title: {
      control: "text",
      defaultValue: "",
    },
    inline: {
      control: "boolean",
    },
    feedback: {
      control: "boolean",
    },
    closable: {
      control: "boolean",
    },
    inverted: {
      control: "boolean",
    },
    default: {
      control: "text",
      description:
        "**[OBRIGATÓRIO]** Transmissão de informação de feedback do sistema.",
    },
  },
};

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-message v-bind="args">${args.default}</br-message>`,
});

export const base = Template.bind({});
base.args = {
  state: "info",
  default: slotDefault,
};

export const feedback = Template.bind({});
feedback.args = {
  state: "info",
  feedback: true,
  default: slotDefault,
};

export const closable = Template.bind({});
closable.args = {
  state: "info",
  closable: true,
  default: slotDefault,
};

export const title = Template.bind({});
title.args = {
  state: "info",
  title: "Texto da mensagem com título.",
  default: slotDefault,
};

export const titleInline = Template.bind({});
titleInline.args = {
  state: "info",
  title: "Texto da mensagem com título inline.",
  inline: true,
  default: slotDefault,
};

export const inverted = Template.bind({});
inverted.args = {
  state: "info",
  inverted: true,
  default: slotDefault,
};
