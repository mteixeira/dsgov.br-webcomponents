/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import BrTab from "./BrTab.ce.vue";
import { kebabiseArgs } from "../../util/Utils.js";

const slotDefault = `
  <br-tab-item title="Sobre" id="panel-1"><p>Sobre</p></br-tab-item>
  <br-tab-item title="Todos" id="panel-2"><p>Todos</p></br-tab-item>
  <br-tab-item title="Notícias" id="panel-3"><p>Notícias</p></br-tab-item>
  <br-tab-item title="Serviços" id="panel-4"><p>Serviços</p></br-tab-item>
`;

const slotComIcone = `
  <br-tab-item title="Sobre" id="panel-1" icon="home"><p>Sobre</p></br-tab-item>
  <br-tab-item title="Todos" id="panel-2" icon="image"><p>Todos</p></br-tab-item>
  <br-tab-item title="Notícias" id="panel-3" icon="image"><p>Notícias</p></br-tab-item>
  <br-tab-item title="Serviços" id="panel-4" icon="image"><p>Serviços</p></br-tab-item>
`;

export default {
  title: "Dsgov/br-tab",
  component: BrTab,
  parameters: {
    backgrounds: {
      default: "light",
      values: [
        { name: "light", value: "#fff" },
        { name: "dark", value: "#071D41" },
      ],
    },
  },
  argTypes: {
    density: {
      control: {
        type: "select",
        options: ["small", "medium", "large"],
      },
    },
    tabNavFontSize: {
      control: {
        type: "range",
        min: 10,
        max: 50,
        step: 1,
      },
    },
    default: {
      description:
        "**[OBRIGATÓRIO]** Conteúdo da notificação, que deve ser passado por slot.",
      control: "text",
      type: {
        required: true,
      },
    },
  },
};

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) };
  },
  template: `<br-tab v-bind="args">${args.default}</br-tab>`,
});

export const Default = Template.bind({});
Default.args = {
  default: slotDefault,
};

export const Icon = Template.bind({});
Icon.args = {
  default: slotComIcone,
};

export const Inverted = Template.bind({});
Inverted.parameters = {
  backgrounds: { default: "dark" },
};
Inverted.args = {
  inverted: true,
  default: slotDefault,
};
