/*
 * This file is part of DSGOV.BR - Webcomponents.
 *
 * DSGOV.BR - Webcomponents is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * at your option) any later version.
 *
 * DSGOV.BR - Webcomponents is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DSGOV.BR - Webcomponents.  If not, see <https://www.gnu.org/licenses/>6.
 *
 * Copyright 2021 Serpro.
 */
import { shallowMount, mount } from "@vue/test-utils";
import BrTab from "./BrTab.ce.vue";
import BrTabItem from "./BrTabItem.ce.vue";

describe("BrTab", () => {
  it("Deve renderizar o componente tab", () => {
    const wrapper = shallowMount(BrTab);
    expect(wrapper.classes(`br-tab`)).toBe(true);
  });

  it("Deve exibir o texto da propriedade aria-label do br-tab", () => {
    const wrapper = shallowMount(BrTab, {
      propsData: { label: "Sobre" },
    });
    const tab = wrapper.find("nav[aria-label]").attributes("aria-label");
    expect(tab).toMatch("Sobre");
  });

  it("Deve aplicar a densidade baixa ao br-tab componente", () => {
    const wrapper = shallowMount(BrTab, {
      propsData: { density: "small" },
    });
    expect(wrapper.classes(`small`)).toBe(true);
  });
});

describe("BrTabItem", () => {
  it("Deve renderizar o componente tabItem", () => {
    const wrapper = shallowMount(BrTabItem);
    expect(wrapper.classes(`tab-panel`)).toBe(true);
  });
});
