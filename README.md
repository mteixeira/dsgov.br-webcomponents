DSGOV.BR - Webcomponents is free software.

Copyright 2021 Serpro, redistribute it and/or modify it under the terms of the LGPL version 3.

See [DSGOV.BR WebSite](https://dsgov.estaleiro.serpro.gov.br/) to learn more about.


# teste-vue-3

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
